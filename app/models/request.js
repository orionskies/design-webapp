// libraries
var mongoose = require('mongoose');

// schema for request model
var requestSchema = mongoose.Schema({
    type        : String,
    URL         : String,
    priority    : Number,
    userID      : String,
    available   : Number
});

// request model
module.exports = mongoose.model('Request', requestSchema);
