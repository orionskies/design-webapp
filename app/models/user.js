// libraries
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

// schema for user model
var userSchema = mongoose.Schema({

    local               : {
        email           : String,
        password        : String,
        name            : String,
        rank            : Number,
        mailUser        : String,
        mailPass        : String,
        mailServer      : String,
        mailCount       : Number,
        mailCountPrev   : Number
    }

});

// methods
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

// user model
module.exports = mongoose.model('User', userSchema);
