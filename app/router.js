// load up the user model
var User       		= require('../app/models/user');

// load up the request model
var Request       	= require('../app/models/request');

module.exports = function(app, passport) {

	// Index route ----------------------------------------------------------------------------------
	app.get('/', function(req, res) {
		// logged in
		if (req.user) {
			res.redirect('/profile');
		} 
		// not logged in
		else {
			res.redirect('/login');
		}
		res.render('index.ejs');
	});

	// login route ----------------------------------------------------------------------------------
	app.get('/login', function(req, res) {
		res.render('login.ejs', { message: req.flash('loginMessage') });
	});

	// process the login form ----------------------------------------------------------------------------------
	app.post('/login', passport.authenticate('local-login', {
		successRedirect : '/profile',
		failureRedirect : '/login',
		failureFlash : true
	}));

	// sign up ----------------------------------------------------------------------------------
	app.get('/signup', function(req, res) {
		res.render('signup.ejs', { message: req.flash('signupMessage') });
	});

	// process the signup form ----------------------------------------------------------------------------------
	app.post('/signup', passport.authenticate('local-signup', {
		successRedirect : '/profile', 
		failureRedirect : '/signup', 
		failureFlash : true 
	}));

	// render user profile ----------------------------------------------------------------------------------
	app.get('/profile', isLoggedIn, function(req, res) {
		var tab = req.param('tab');
		// check for notifications
		Request.find({userID: req.user.id, available: 2}, function(err, available) {
			res.render('profile.ejs', {
				user : 		req.user,
				requests: 	available,
				tab:		tab,
				message: 	"Logged in!"
			});
		});

	});

	// clear notification ----------------------------------------------------------------------------------
	app.post('/clearnotification', isLoggedIn, function(req, res) {
		Request.findOne({ _id: req.body.ID }, function(err, request) {
			if (err) throw err;
			request.available = 3;
  			request.save();
			console.log("Cleared notification.");
    	});
		res.send("Cleared notification.");
	});

	// render emails ----------------------------------------------------------------------------------
	app.get('/email', isLoggedIn, function(req, res) {
		
		var fs = require('fs');
		try {
			var obj = JSON.parse(fs.readFileSync('data/emails/' + req.user._id + '/email.json', 'utf8'));
			console.log("Email count is: " + obj.emails.length);

			// Update email count
			User.findOne({ _id: req.user.id }, function(err, user) {
				if (err) throw err;
				if (user.local.mailCount == user.local.mailCountPrev) {
					user.local.mailCount += obj.emails.length;
				}
	  			user.save();
				console.log("Email count updated - received new email.");
	    	});

			// Render template
			res.render('email.ejs', {
				user : req.user, 
				emails: obj.emails
			});
		} 
		catch (err) {
			res.render('email.ejs', {
				user : req.user, 
				emails: null
			});
		}
		
	});

	// render data view ----------------------------------------------------------------------------------
	app.get('/data', isLoggedIn, function(req, res) {

		const fs = require('fs');
		const fileDir = 'views/files/' + req.user._id;

		Request.find({userID: req.user.id, type: "site", available: { $in: [2, 3] }}, function(err, available) {
			fs.readdir(fileDir, function(err, files) {
				res.render('data.ejs', {
					user : req.user,
					files : files,
					sites: available
				});
			})
		});

	});

	// render settings ----------------------------------------------------------------------------------
	app.get('/system', isLoggedIn, function(req, res) {
		
		Request.count({available: 1}, function(err, count_prequests){
			Request.count({}, function(err, count_requests){
				User.count({}, function(err, count_users){
					Request.find({type: "site", available: { $gte: 2}}, function(err, websites) {
						console.log(websites);
						res.render('system.ejs', {
							user : req.user,
							usercount : count_users.toString(),
							reqcount : count_requests.toString(),
							preqcount : count_prequests.toString(),
							websites: websites
						});
					});
				});
			});
		});

	});

	// all requests by logged in user
	app.get('/requests', isLoggedIn, function(req, res) {
		Request.find({userID: req.user.id, available: 1}, function(err, requests) {
			res.render('requests.ejs', {
				user : req.user,
				requests: requests
			});
		}).sort({priority: 1});
	});

	// new data request ----------------------------------------------------------------------------------
	app.get('/requests/new', isLoggedIn, function(req, res) {
		var url = req.param('url');
		// url parameters provided
		if (url) {
			res.render('new_request.ejs', {
				user : req.user,
				url  : url 
			});
		}
		// no parameters
		else {
			res.render('new_request.ejs', {
				user : req.user 
			});
		}
	});

	// logout route ----------------------------------------------------------------------------------
	app.get('/logout', function(req, res) {
		req.logout();
		res.redirect('/');
	});

	// Get current notification count ----------------------------------------------------------------------------------
	app.get('/notificationCount', function(req, res){
		// check if the user exists
		if (req.user == null) {
			res.send("invalid id");	
		}
		else {
			Request.count({userID: req.user.id, available: 2}, function(err, notifications){
				res.send(notifications.toString());
			});
		}
	});

	// 404 - last route (assumes logged in user) ----------------------------------------------------------------------------------
	app.get('*', isLoggedIn, function(req, res){
		res.render('404.ejs', {
			user : req.user 
		});
	});

	// update user records ----------------------------------------------------------------------------------
	app.post('/update', isLoggedIn, function(req, res) {
		User.findOne({ _id: req.user.id }, function(err, user) {
			if (err) throw err;
			user.local.name = req.body.name;
			user.local.email = req.body.email;
			user.local.mailUser = req.body.mailUser;
			user.local.mailPass = req.body.mailPass;
			user.local.mailServer = req.body.mailServer;
  			user.save();
			console.log("Record updated.");
    	});
	});

	// create new request ----------------------------------------------------------------------------------
	app.post('/newrequest', isLoggedIn, function(req, res) {
		// Update url for an email request
		if (req.body.type == "mail") {
			req.body.URL = req.user.local.mailUser + "," + req.user.local.mailPass + "," + req.user.local.mailServer + "," + req.user.local.mailCount;

			// Update email count
			User.findOne({ _id: req.user.id }, function(err, user) {
				if (err) throw err;
				user.local.mailCountPrev = user.local.mailCount;
	  			user.save();
				console.log("Email count updated - new email request.");
	    	});
		}

		Request.findOne({type: "site", URL: req.body.URL}, function(err, exists) {
			if (exists) {
				console.log("Error: Duplicate request.");
				res.send("Error: Duplicate request.<br>You may visit existing website by going to <a target='_blank' href='"+req.body.URL+"'>"+req.body.URL+"</a>");
			}
			else {
				// find the lowest priority
				Request.findOne({userID: req.user.id}, function(err, request) {
					if (err) throw err;
					// other requests exist
					if (request) {
						// create new request object and populate fields from html form
						var newRequest = new Request({ 
								type: 		req.body.type, 
								URL: 		req.body.URL, 
								priority: 	request.priority+1,
								userID: 	req.user.id,
								available:	0 
						});
					}
					// first request
					else {
						// create new request object and populate fields from html form
						var newRequest = new Request({ 
								type: 		req.body.type, 
								URL: 		req.body.URL, 
								priority: 	1,
								userID: 	req.user.id,
								available:	0 
						});
					}
					// save the document to collection
					newRequest.save(function (err, newRequest) {
						if (err) return console.error(err);
						// success
						res.send("Request added successfully.");
						console.log("Request added successfully.");
					});
				}).sort({priority: -1});
			}
		});
	});

	// update request priority ----------------------------------------------------------------------------------
	app.post('/updatePriority', isLoggedIn, function(req, res) {
		req.body.order.forEach(function(item, i) {
			Request.findOne({ _id: item }, function(err, request) {
				if (err) throw err;
				request.priority = i+1;
				request.save();
    		});
		});
		res.send("Order priority updated.");
		console.log("Order priority updated.");
	});

};

// check for authentication
function isLoggedIn(req, res, next) {
	// if user is authenticated in the session, carry on
	if (req.isAuthenticated()) {
		return next();
	}
	res.redirect('/');
}
