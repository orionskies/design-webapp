// AJAX for Updating database entries
$("#updateProfile").submit(function(e) {

    $.ajax({
        type: "POST",
        url: "/update",
        data: {
            name : $("input[name='name']").val(),
            email : $("input[name='email']").val(),
            mailUser : $("input[name='mailUser']").val(),
            mailPass : $("input[name='mailPass']").val(),
            mailServer : $("input[name='mailServer']").val()
        },
        success: function(data) {
            location.reload(); // CHANGE LATER!
        }
    });

    e.preventDefault(); // avoid to execute the actual submit of the form.

});

// AJAX for creating new request
$("#processRequest").submit(function(e) {

    $.ajax({
        type: "POST",
        url: "/newrequest",
        data: {
            type : $("input[name='req']").val(),
            URL : $("input[name='URL']").val(),
            priority : $("input[name='priority']").val()    // remove this in the future
        },
        success: function(data) {
            $('[name="URL"]').val(""); // clear URL
            noty({
                text: data,
                timeout: 5000,
                layout: 'bottomRight',
                theme: 'relax',
                progressBar: true,
            });
        }
    });

    e.preventDefault(); // avoid to execute the actual submit of the form.

});

// Ajax notifications check
function ncheck() {
    $.ajax({
        context: this,
        type: 'GET',
        url: '/notificationCount',
        success: function(data) {
            if (data != 0) {
                $("#ncount").show();
                $("#ncount").html(parseInt(data));
            }
            else {
                $("#ncount").hide();
            }
            
        }
    });
}

$(document).ready(function() {

    // Transitions
    $('#profile_info').hide().transition('scale');

    // Check notifications
    ncheck();

    // Tabs
    $('.menu .item').tab();

    // Checkboxes
    $('.ui.radio.checkbox').checkbox();

    // Dropdowns
    $('.ui.dropdown').dropdown();

    // Popups
    $('.popup').popup();

    // Accordions
    $('.ui.accordion').accordion();

    // Random Quote Generator
    var quotes = [
        "I don't want to come off as arrogant here, but I'm the greatest botanist on this planet.", 
        "They say once you grow crops somewhere, you have officially colonized it. So, technically, I colonized Mars. In your face, Neil Armstrong!", 
        "I admit it's fatally dangerous, but I'd get to fly around like Iron Man.", 
        "If we are going to have a secret project called 'Elrond', then I want my code name to be 'Glorfindel'.", 
        "In the face of overwhelming odds, I'm left with only one option, I'm gonna have to science the **** out of this."
    ];
    var counter = Math.round(Math.random()*3) + 1;
    $(".quote").html('"' + quotes[counter] + '"');

    // Tiny rovers
    ltor = "+=" + $(document).width()/2.03;
    rtol = "-=" + $(document).width()/2.03;

    var offset= $(document).height()-$(".ui.footer").height()-48;
    $(".rover").css("top", offset);
    $(".rover2").css("top", offset);
    $(".rover").addClass('flipped');
    $(".rover").animate({ left: ltor }, 5000, function() {
        $(".rover").attr('src', '/img/rover2.png');
        $(".rover").removeClass('flipped');
        $(".rover").animate({ left: rtol }, 5000);
        //$(".rover").popup('show');
        //setTimeout("$('.rover').popup('hide');", 1300);
    });
    $(".rover2").animate({ left: rtol }, 5000, function() {
        $(".rover2").attr('src', '/img/rover.png');
        $(".rover2").addClass('flipped');
        $(".rover2").animate({ left: ltor }, 5000);
    });

});