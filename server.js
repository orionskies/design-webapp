
// libraries
var express  = require('express');
var app      = express();
var port     = process.env.PORT || 8080;
var mongoose = require('mongoose');
var passport = require('passport');
var flash    = require('connect-flash');

var configDB = require('./config/database.js');

// configuration info
mongoose.connect(configDB.url); // connect to db

require('./config/passport')(passport);

app.configure(function() {

	// set up express
	connect = require('connect');
	app.use(express.logger('dev')); // log requests to console
	app.use(express.logger({ 
		format: ':remote-addr' } // log remote IP
	));
	app.use(express.cookieParser()); // read cookies

	app.use(express.static('views'));
	app.use(connect.json());
	app.use(connect.urlencoded());
	
	// set up templating engine
	app.set('view engine', 'ejs'); // set up ejs for templating

	// set up passport
	app.use(express.session({ secret: 'redplanet' })); // session secret
	app.use(passport.initialize());
	app.use(passport.session()); // persistent login sessions
	app.use(flash()); // flash messages stored in session

});

require('./app/router.js')(app, passport); // pass routes

// run server
app.listen(port);
console.log('-------------------------------');
console.log('Houston, we have a server: ' + port + '\n');
